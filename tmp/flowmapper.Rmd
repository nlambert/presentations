---
title: <br/><br/></img><br/><font size='60'>Cartographier les flux dans R</font>
subtitle: <font size='6'>(mais pas que)</font>
author: "<br><ins>#WorkInProgress</ins><br/><br/><br/><br/> Par Françoise Bahoken & Nicolas Lambert"
output: 
  revealjs::revealjs_presentation:
    classoption: "notitlepage"
    self_contained: true
    theme: "night"
    transition: "convex"
    background_transition: "convex"
    css : "css/style.css"
---

```{eval = FALSE, echo = TRUE, fig.height=8}
library(ttt)
library(sf)

# Import
crs <- "+proj=robin +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"

subregions <- st_read(system.file("subregions.gpkg", package="flowmapper")) %>% st_transform(crs)
migr <- read.csv(system.file("migrantstocks2019.csv", package="flowmapper"))

#  Selection, filtrage, traitement en amont

threshold <- 1500
migr <- migr[migr$fij >= threshold,]
```


# <iframe width="100%" height=1000 frameborder="0" style="background-color: white"
  src="https://observablehq.com/embed/@neocartocnrs/arrows?cells=viewof+x1coord%2Cviewof+y1coord%2Cviewof+x2coord%2Cviewof+y2coord%2Cviewof+r1%2Cviewof+r2%2Cviewof+thickness%2Cviewof+type%2Cmap"></iframe>

## <iframe width="100%" height="1135" frameborder="0" style="background-color: white"
  src="https://observablehq.com/embed/@neocartocnrs/flow-map?cells=viewof+f1%2Cviewof+f2%2Cviewof+threshold%2Cmap"></iframe>  
  
## <iframe width="100%" height="1135" frameborder="0" style="background-color: white"
  src="https://analytics.huma-num.fr/Nicolas.Lambert/migrexplorer3/"></iframe>  
  
  
  
## {data-background-iframe="https://analytics.huma-num.fr/Nicolas.Lambert/migrexplorer3/"}

# <br/><br/><br/><br/><br/><br/>Merci<br/>

